# Build hledger for aarch64

This is a `Dockerfile` to build [`hledger`](https://hledger.org) in the `fedora` container, which works for aarch64 (Raspberry Pi, SailfishOS, etc...)

## Usage

Run the following in this repository on the same architecture you want to build for (e.g. on an `aarch64` machine if you want an `aarch64`-hledger):

```bash
# Run the build script
./build.sh
```
