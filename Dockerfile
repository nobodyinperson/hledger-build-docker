FROM alpine
MAINTAINER Yann Büchau <nobodyinperson@posteo.de>

ENV LC_ALL=C.UTF-8
RUN apk update
RUN apk add ghc cabal wget musl-dev libffi-dev libffi ncurses-dev ncurses llvm clang gcc
RUN cabal update
# might specify hledger-VERSION for specific version (leaving the version let's cabal decide with the latest buildable version is)
RUN cabal install hledger
RUN ~/.cabal/bin/hledger --version 
