#!/bin/sh -e
dockerfile="$1"
test -n "$dockerfile" || dockerfile=Dockerfile-fedora
sudo docker build -t hledger - < "$dockerfile"
hledger_version="$(sudo docker run --rm -it hledger /root/.cabal/bin/hledger --version | perl -pe 's|[^a-zA-Z0-9.-]+|-|g;s|-+|-|g;s|-+$||g')-${dockerfile#Dockerfile-}"
sudo docker tag hledger hledger/$hledger_version
echo "Built hledger version $hledger_version"
echo "Extracting $hledger_version from container..."
sudo docker rm -f hledger-extract || true
sudo docker create --name hledger-extract hledger
sudo docker cp -L hledger-extract:/root/.cabal/bin/hledger "$hledger_version"
sudo docker rm -f hledger-extract || true
ln -sf "$hledger_version" hledger
echo "Zipping for GitHub upload..."
(set -x;gzip -vfk "$hledger_version")
(set -x;./hledger --version)
